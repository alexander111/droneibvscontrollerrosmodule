#include <stdio.h>
#include <iostream>

//ROS
#include "ros/ros.h"


//Controller
#include "DroneIBVSController.h"

//ROSModule and ROSMsgs
#include "droneModuleROS.h"
#include "droneMsgsROS/dronePitchRollCmd.h"
#include "droneMsgsROS/droneDYawCmd.h"
#include "droneMsgsROS/droneDAltitudeCmd.h"
#include "droneMsgsROS/droneCommand.h"
#include "geometry_msgs/Vector3Stamped.h"
#include "std_msgs/Bool.h"
#include "std_msgs/Float32.h"
#include "droneMsgsROS/BoundingBox.h"
#include "droneMsgsROS/imageFeaturesIBVS.h"
#include "droneMsgsROS/imageFeaturesFeedbackIBVS.h"

//OpenCV
#include <opencv2/highgui/highgui.hpp>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>

class DroneIBVSControllerROSModule : public DroneModule
{



public:
     DroneIBVSControllerROSModule();
     ~DroneIBVSControllerROSModule();

public:
     DroneIBVSController MyDroneIBVSController;

public:
   //  DroneTrackerEyeROSModule MyOpenTLDInterface;

public:
     void init();
     void close();
     void readParameters();

protected:
     bool resetValues();
     bool startVal();
     bool stopVal();

public:

     bool run();

public:
     void open(ros::NodeHandle & nIn, std::string ModuleName);

private:
     std::string configFile;

     //Subscribers
private:

     ros::Subscriber drone_rotation_angle_sub;
     ros::Subscriber tracking_object_sub;
     ros::Subscriber is_object_on_frame_sub;
     ros::Subscriber get_bounding_box_sub;

     void droneRotationAnglesCallback(const geometry_msgs::Vector3Stamped& msg);
     void trackingObjectCallback(const std_msgs::Bool& msg);
     void isObjectOnFrameCallback(const std_msgs::Bool& msg);
     void getBoundingBoxCallback(const droneMsgsROS::BoundingBox& msg);

      //Publishers
private:

     ros::Publisher drone_command_pitch_roll_publisher;
     ros::Publisher drone_command_dyaw_publisher;
     ros::Publisher drone_command_daltitude_publisher;
     ros::Publisher drone_image_features_publisher;
     ros::Publisher drone_image_features_feedback_publisher;

     void dronePublishPitchRollCmds();
     void dronePublishDaltitudeCmds();
     void dronePublishDyawCmds();
     void dronePublishImageFeatures();
     void dronePublishImageFeaturesFeedback();

public:
     cv_bridge::CvImagePtr cvFrontImage;
     cv::Mat cam_image_modified,frontcameraimage;

public:

     droneMsgsROS::droneNavCommand DroneNavCommands;
     int dronePublishIBVSControllerCmds();

//topic Names
public:
//subsribers
     std::string rotation_angles_topic_name;
     std::string tracking_object_topic_name;
     std::string is_object_on_frame_topic_name;
     std::string get_bounding_box_topic_name;

//publishers
     std::string command_pitch_roll_topic_name;
     std::string command_dAltitude_topic_name;
     std::string command_dYaw_topic_name;
     std::string controller_image_features_topic_name;
     std::string controller_image_features_feedback_topic_name;

     droneMsgsROS::dronePitchRollCmd  pitchrollcmdsmsg;
     droneMsgsROS::droneDAltitudeCmd daltitudecmdsmsg;
     droneMsgsROS::droneDYawCmd dyawcmdmsg;

public:

     double droneYaw, dronePitch, droneRoll;
     float  fxs,  fys,  fss,   fDs;   // image feature measurements
     float fxs_4DyC, fxs_4DYC, fys_4DzC, fDs_4DxC;
     float fxci = 0.0, fyci = 0.0, fsci = 0.1, fDci = 1.0/sqrt(0.1);
     bool tracker_is_tracking, tracker_object_on_frame, isControllerStarted;

public:

     ros::Time bb_timestamp;
     int     bb_x, bb_y, bb_width, bb_height;
     float   bb_confidence;
     bool    bb_object_is_on_frame = false;

public:

    void setNavCommand(float pitch, float roll, float dyaw, float dz);
    void setNavCommandToZero();

};

